<?php
/**
 * Plugin Name: Creative Cache Pinterest Pin Generator
 * Plugin URI: https://morganmspencer.com
 * Description: This plugin generates a Pinterest Pin for your posts
 * Version: 1.0.0
 * Author: Morgan Spencer
 * Author URI: https://morganmspencer.com
 */

defined( 'ABSPATH' ) or die( 'No script please!' );

//Create Pinterest Directory

$upload_dir = wp_upload_dir(); 
$user_dirname = $upload_dir['basedir'] . '/pinterest';
if(!file_exists($user_dirname)) wp_mkdir_p($user_dirname);

add_action('admin_menu', 'mmsppg_plugin_setup_menu');
function mmsppg_plugin_setup_menu(){
        add_menu_page( 'Pinterest Pin Generator', 'Pin Generator', 'manage_options', 'mmsppg-plugin', 'mmsppg_admin_init' );
}

function mmsppg_admin_init(){
        include( plugin_dir_path( __FILE__ ) . 'php/admin.php');
}

function mmsppg_adding_scripts() {
	wp_enqueue_style('mmsppg-styles', plugin_dir_url(__FILE__) . 'css/styles.css?v=1.0.4');
	wp_enqueue_script('html2canvas', plugin_dir_url(__FILE__) . 'js/html2canvas.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'mmsppg-scripts', plugin_dir_url(__FILE__) . 'js/scripts.js?v=1.0.3', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'mmsppg_adding_scripts' );

// register Pinterest Pin Widget
add_action( 'widgets_init', function(){
	register_widget( 'mmsppg_widget' );
});

class mmsppg_widget extends WP_Widget {
	// class constructor
	public function __construct() {
	    $widget_ops = array( 
    		'classname' => 'mmsppg_widget',
    		'description' => 'Display a clickable Pinterest Pin for your post.',
    	);
    	parent::__construct( 'mmsppg_widget', 'Pinterest Pin Generator', $widget_ops );
	}
	
	// output the widget content on the front-end
	public function widget( $args, $instance ) {
		if ( has_post_thumbnail() && is_single() ) {
			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
			}

			include( plugin_dir_path( __FILE__ ) . 'php/pin.php');
		}
	}

	// output the option form field in admin Widgets screen
	public function form( $instance ) {
	    $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Title', 'text_domain' );
    	?>
    	<p>
    	<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
    	<?php esc_attr_e( 'Title:', 'text_domain' ); ?>
    	</label> 
    	
    	<input 
    		class="widefat" 
    		id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
    		name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
    		type="text" 
    		value="<?php echo esc_attr( $title ); ?>">
    	</p>
	<?php
	}

	// save options
	public function update( $new_instance, $old_instance ) {
	    $instance = array();
    	$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    		
    	$selected_posts = ( ! empty ( $new_instance['selected_posts'] ) ) ? (array) $new_instance['selected_posts'] : array();
    	$instance['selected_posts'] = array_map( 'sanitize_text_field', $selected_posts );
    
    	return $instance;
	}
}