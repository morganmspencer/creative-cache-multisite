jQuery(document).ready(function($) {	
	if ($('#mmsppg-capture').length) {
		html2canvas(document.querySelector("#mmsppg-capture"), {scale: 2, allowTaint: false, logging: true, useCORS: true,}).then(canvas => {

			var imagedata = canvas.toDataURL('image/png'),
				imgdata = imagedata.replace(/^data:image\/(png|jpg);base64,/, ""),
				titleslug = $('#mmsppg-pinterest-image').attr('data-slug'),
				pinteresturl = $('#mmsppg-pinterest-image').attr('href'),
				templateurl = $('#mmsppg-pinterest-image img').attr('data-template-uri'),
				imgurl = $('#mmsppg-pinterest-image img').attr('src');
			//ajax call to save image inside folder
			$.ajax({
				url: templateurl + '/send.php',
				data: {
					imgdata: imgdata,
					imgurl: imgurl,
					titleslug: titleslug,
				},
				type: 'post',
				success: function (response) {
					$('#mmsppg-pinterest-image img').attr('data-template-uri', '');
					$('#mmsppg-pinterest-image').attr('href', pinteresturl + imgurl + response);
					$('#mmsppg-pinterest-image img').attr('src', imgurl + response);
					$('#mmsppg-pinterest-image-generator').show();  
				}
			});

		});
	}
});