<h1>Pinterest Pin Generator</h1>
<p>created by <a href="https://morganmspencer.com/" target="_blank">Morgan Spencer</a></p>
<hr>
<div class="mmsppg-setting-container">
	<?php 
	// the query
	$the_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1, 'orderby'	=> 'name', 'order' 	=> 'ASC',)); ?>
	<p class="mmsppg-loading-text"></p>
<!-- 	<button id="mmsppgAdd" class="mmsppg-button">Generate images</button> -->
	<button id="mmsppgClear" class="mmsppg-button">Clear all images</button>
	
	<?php if ( $the_query->have_posts() ) : ?>

	<ul>

		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php 
				global $post;
				$post_slug = $post->post_name;
			?>
			<li><p class="mmsppg-post-item"><?php the_title(); ?> <a href="#" class="mmsppg-clear-post" data-slug="<?php echo $post_slug; ?>">Clear</a></p></li>
		<?php endwhile; ?>
		<!-- end of the loop -->

	</ul>

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
	
</div>

<style type="text/css">
	#mmsppgAdd {
		background-color: green;
	}
	
	#mmsppgClear {
		background-color: red;
	}
	
	.mmsppg-button {
		border: none;
		padding: 5px;
		font-size: 14px;
		color: #fff;
		cursor: pointer;
		text-transform: uppercase;
		font-weight: bold;
	}
	
	.mmsppg-button[disabled="disabled"] {
		background-color: #d8d8d8 !important;
	}
	
	.mmsppg-post-item {
		margin: 0;
	}
	
</style>

<script type="text/javascript">
jQuery(document).ready(function($) {
	
	function mmsppgClearImages() {
		var pluginurl = '<?php echo plugin_dir_path( __FILE__ ) ?>',
			loadingText = 'Clearing all generated Pinterest Pin images...';
		
		$('.mmsppg-loading-text').html('');
		$('.mmsppg-loading-text').append(loadingText);
		$('.mmsppg-button').attr('disabled', 'disabled');
		
		$.ajax({
			url: pluginurl + 'clear.php',
			type: 'post',
			success: function (response) {
				 $('.mmsppg-loading-text').html(response);
				$('.mmsppg-button').prop("disabled", false);
			}
		});
	}
	
	function mmsppgClearPost($titleslug) {
		var pluginurl = '<?php echo plugin_dir_path( __FILE__ ) ?>',
			loadingText = 'Clearing the Pinterest Pin image...',
			titleslug = $titleslug;
		
		$('.mmsppg-loading-text').html('');
		$('.mmsppg-loading-text').append(loadingText);
		$('.mmsppg-button').attr('disabled', 'disabled');
		
		$.ajax({
			url: pluginurl + 'clear-post.php',
			type: 'post',
			data: {
				titleslug: titleslug,
			},
			success: function (response) {
				 $('.mmsppg-loading-text').html(response);
				$('.mmsppg-button').prop("disabled", false);
			}
		});
	}
	
	function mmsppgGenerateImages() {
		var pluginurl = '<?php echo plugin_dir_path( __FILE__ ) ?>',
			loadingText = 'Generating all Pinterest Pin images...';
		
		$('.mmsppg-loading-text').html('');
		$('.mmsppg-loading-text').append(loadingText);
		$('.mmsppg-button').attr('disabled', 'disabled');
		
		$.ajax({
			url: pluginurl + 'generate.php',
			type: 'post',
			success: function (response) {
				 $('.mmsppg-loading-text').html(response);
				$('.mmsppg-button').prop("disabled", false);
			}
		});
	}
	
	$('#mmsppgAdd').click(function() {
		mmsppgGenerateImages();
	});
	
	$('#mmsppgClear').click(function() {
		mmsppgClearImages();
	});
	
	$('.mmsppg-clear-post').click(function() {
		var titleslug = $(this).attr('data-slug')
		mmsppgClearPost(titleslug);
	});
});

</script>