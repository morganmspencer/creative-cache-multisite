<?php

$successMessage = 'Images have been successfully cleared!';

//The name of the folder.
$upload_dir = wp_upload_dir(); 
$folder = $upload_dir['basedir'] . '/pinterest';
 
//Get a list of all of the file names in the folder.
$files = glob($folder . '/*');
 
//Loop through the file list.
foreach($files as $file){
    //Make sure that this is a file and not a directory.
    if(is_file($file)){
        //Use the unlink function to delete the file.
        unlink($file);
    }
}

echo $successMessage;

?>