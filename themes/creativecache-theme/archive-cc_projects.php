<?php
/*
Template Name: All Projects
*/
?>
<?php get_header(); ?>

<article id="main-content" class="container">
    <h1 class="entry-title">Selected Projects</h1>
    <div class="projects-controls">
        <a href="" class="expand-all" title="Toggle Fullscreen" role="button"><i class="fas fa-expand"></i></a>
        <?php
            $taxonomy = 'projects_categories';
            $terms = get_terms($taxonomy);
        
            if ( $terms && !is_wp_error( $terms ) ) :
        ?>
        <select id="project-type">
            <option value="all">All</option>
            <?php foreach ( $terms as $term ) { ?>
                    <option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
            <?php
                }
            ?>
        </select>
        <?php endif; ?>
    </div>
	<?php 
		$args = array(
		'post_type' => 'cc_projects',
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		);
	$the_pages = new WP_Query( $args );

	if( $the_pages->have_posts() ): while( $the_pages->have_posts() ): $the_pages->the_post();
	    $terms = get_the_terms( $post->ID , 'projects_categories' );
	    $post_thumbnail_id = get_post_thumbnail_id( $post_id );
		$imgmeta = wp_get_attachment_metadata( $post_thumbnail_id );
	?>
	
	<section class="website-window<?php if ( $terms && !is_wp_error( $terms ) ) : foreach( $terms as $term ):?> <?php echo $term->slug;?><?php endforeach; endif; ?>">
	    <div class="web-window minimized<?php if ($imgmeta['width'] > $imgmeta['height']) : ?> image<?php else: ?> website<?php endif; ?>">
	        <div class="top-bar">
	            <div class="bar-button red"></div>
	            <div class="bar-button yellow" title="Minimize" role="button"></div>
	            <div class="bar-button green" title="Expand" role="button"></div>
	        </div>
    	    <div class="website">
    	        <?php the_post_thumbnail( 'full' ); ?>
    	    </div>
	    </div>
	    <div class="project-content">
	        <h2 class="entry-title"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h2>
	        <?php if ( $terms && !is_wp_error( $terms ) ) : 
	        ?>
	        <p class="project-type"><?php foreach( $terms as $term ):?> <?php echo implode(", ", $term->name);?><?php endforeach;?></p>
	        <?php endif; ?>
    	    <div class="project-text"><?php echo the_excerpt(); ?></div>
    	    <a href="<?php echo the_permalink(); ?>">View Project</a>
    	</div>
	</section>
	<?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</article>

<?php get_footer(); ?>