jQuery(document).ready(function($) {
	function footerPush() {
		var footerHeight = $('#footer').outerHeight();
		
		$('.main-container').css('padding-bottom', footerHeight);
	}
	
	$('#primary-nav .menu li.menu-item-has-children > a').each(function() {
		$(this).append('<i class="fas fa-chevron-down"></i>');
	});
	
	$('.mobile-menu').click(function(e) {
		e.preventDefault();
		$('#primary-nav .menu').stop().slideToggle(500);
	});
	
	$('.bar-button.yellow').click(function() {
		if(!$('body').hasClass('page-template-project')) {
		   $(this).parents('.web-window').addClass('minimized');
		}
	})
	
	$('.bar-button.green').click(function() {
		if(!$('body').hasClass('page-template-project')) {
		   $(this).parents('.web-window').removeClass('minimized');
		}
	})
	
	$('#primary-nav .menu li.menu-item-has-children > a').click(function(e) {
		if($('.mobile-menu').is(":visible")) {
			if($(this).next('.sub-menu').is(":hidden")) {
				e.preventDefault();
				$(this).next('.sub-menu').slideDown(500);
			}
		}
	});
	
	$('#page').click(function() {
		if($('.mobile-menu').is(":visible")) {
			if($('#primary-nav .menu').is(":visible")) {
				$('#primary-nav .menu').slideUp(500);
			}
		}
	});
	
	$('#banner').click(function() {
		if($('.mobile-menu').is(":visible")) {
			if($('#primary-nav .menu').is(":visible")) {
				$('#primary-nav .menu').slideUp(500);
			}
		}
	});
	
	$('.expand-all').click(function(e) {
		e.preventDefault();
		if($(this).children('i').hasClass('fa-expand')) {
			$('.web-window').removeClass('minimized');
			$(this).children('i').toggleClass('fa-expand fa-compress');
		} else {
			$('.web-window').addClass('minimized');
			$(this).children('i').toggleClass('fa-expand fa-compress');
		}
	});
	
	$('#project-type').change(function() {
		var selected = $(this).find( 'option:selected' ).val();
		
		console.log(selected);
		
		if ( selected == 'all') {
			$('.website-window').each(function() {
				$(this).show();
			});
		} else {
			$('.website-window').each(function() {
				
				if ($(this).hasClass(selected)) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		}
	});
	
	footerPush();
	
	$(window).resize(function() {
		if($('.mobile-menu').is(":visible")) {
			$('#primary-nav .menu').css('display', 'none');
		} else {
			$('#primary-nav .menu').css('display', 'block');
		}
		
		footerPush();
	});
});