<?php

//Enqueue scripts and styles
function citadel_adding_scripts() {
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'citadel_adding_scripts' );

add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );
add_theme_support( 'title-tag' );
add_post_type_support( 'page', 'excerpt' );

register_nav_menus( array(
	'primary' 		=> 'Main Menu',
	'secondary' 	=> 'Secondary Menu',
	'social' 		=> 'Social Menu',
	'legalfooter' 	=> 'Legal Footer Menu',
) );

register_sidebar(array(
	'name' 			=> 'Blog Sidebar',
	'id'            => 'blog-sidebar',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Right Sidebar',
	'id'            => 'right-sidebar',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Footer 1',
	'id'            => 'footer-sidebar-one',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Footer 2',
	'id'            => 'footer-sidebar-two',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Footer 3',
	'id'            => 'footer-sidebar-three',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Footer 4',
	'id'            => 'footer-sidebar-four',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

add_filter( 'document_title_separator', 'cc_document_title_separator' );
function cc_document_title_separator( $sep ) {
    $sep = "|";
    return $sep;
}

// Add Projects post type
function create_post_type() {
  register_post_type( 'cc_projects',
    array(
		'labels' => array(
			'name' => __( 'Projects' ),
			'singular_name' => __( 'Project' ),
			'add_new'            => __( 'Add New Project' ),
    		'add_new_item'       => __( 'Add New Project' ),
    		'edit_item'          => __( 'Edit Project' ),
    		'new_item'           => __( 'Add New Project' ),
    		'view_item'          => __( 'View Project' ),
    		'search_items'       => __( 'Search Project' ),
    		'not_found'          => __( 'No projects found' ),
    		'not_found_in_trash' => __( 'No projects found in trash' )
		),
		'public' => true,
        'publicly_queryable' => true,
		'has_archive' => true,
		'hierarchal' => false,
		'rewrite' => array('slug' => 'projects','with_front' => false,),
        'menu_icon' => 'dashicons-portfolio',
        'show_in_rest' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    )
  );
}
add_action( 'init', 'create_post_type' );

function create_projects_taxonomies() {
	register_taxonomy(
		'projects_categories',
		'cc_projects',
		array(
			'label' => __( 'Categories' ),
			'rewrite' => array( 'slug' => 'projects/categories', 'with_front' => false, ),
			'hierarchical' => true,
			'show_in_rest' => true,
		)
	);
}
add_action( 'init', 'create_projects_taxonomies' );

function add_projects_metaboxes() {
	add_meta_box(
		'cc_projects_type',
		'Project Main Type',
		'cc_projects_type',
		'cc_projects',
		'side',
		'default',
		array(
            '__block_editor_compatible_meta_box' => true,
        )
	);
	
	add_meta_box(
        'cc_projects_gallery', // $id
        'Project Gallery', // $title
        'cc_projects_gallery', // $callback
        'cc_projects', // $page
        'side', // $context
        'high', // $priority
        array(
            '__block_editor_compatible_meta_box' => true,
        )
    );
}
add_action( 'add_meta_boxes', 'add_projects_metaboxes' );

function cc_projects_type() {
	global $post;
	// Nonce field to validate form request came from current site
	wp_nonce_field( basename( __FILE__ ), 'project_fields' );
	// Get the location data if it's already been entered
	$project_type = get_post_meta( $post->ID, 'location', true );
	// Output the field
	echo '<input type="radio" name="project_type" value="website" class="widefat">Website<br>';
	echo '<input type="radio" name="project_type" value="image" class="widefat">Image';
}