<?php get_header(); ?>
<header class="entry-header">
	<h1 class="entry-title"><?php bloginfo(); ?> Blog</h1>
</header><!-- .entry-header -->

<article id="main-content" class="container<?php if (is_active_sidebar('blog-sidebar')) : ?> with-sidebar<?php endif; ?>">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	    <header class="entry-header">
			<a href="<?php echo get_permalink(); ?>"><?php the_title( '<h2 class="post-title">', '</h2>' ); ?></a>
		</header><!-- .entry-header -->
		<?php if ( has_post_thumbnail() ) : ?>
		<div class="featured-image">
			<?php the_post_thumbnail(); ?>
		</div>
		<?php endif; ?>
		<div class="blog-entry-content">
    		<p class="entry-meta">by <?php the_author();?></p>
    		<div class="entry-content">
    			<?php
    				echo the_excerpt();
    			?>
    		</div><!-- .entry-content -->
    	</div>
	</div><!-- #post-## -->
	<?php endwhile; ?>
	<?php endif; ?>
</article>

<?php if (is_active_sidebar('blog-sidebar')) : ?>
<aside id="right-sidebar" class="sidebar">
	<?php dynamic_sidebar('blog-sidebar'); ?>
</aside>
<?php endif; ?>
<?php get_footer(); ?>