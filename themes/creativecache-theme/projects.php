<?php
/*
Template Name: All Projects
*/
?>
<?php get_header(); ?>

<article id="main-content" class="container">
    <h1 class="entry-title"><?php echo the_title(); ?></h1>
    <div class="projects-controls">
        <a href="" class="expand-all" title="Toggle Fullscreen" role="button"><i class="fas fa-expand"></i></a>
        <select id="project-type">
            <?php
                $values = get_field('project_type');
                $field = get_field_object('project_type');
                $choices = $field['choices'];
                foreach ($values as $value => $label) {
                    echo '<option value="' . $label . '">' . $label . '</option>';
                }
            ?>
        </select>
    </div>
	<?php 
		$args = array(
		'post_type' => 'page',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => '_wp_page_template',
				'value' => 'project.php'
			)
		)
		);
	$the_pages = new WP_Query( $args );

	if( $the_pages->have_posts() ): while( $the_pages->have_posts() ): $the_pages->the_post();
	?>
	
	<?php $project_types = get_field('project_type'); ?>
	
	<section class="website-window<?php if( $project_types ): foreach( $project_types as $project_type ):?> <?php echo $project_type['label'];?><?php endforeach; endif; ?>">
	    <div class="web-window minimized">
	        <div class="top-bar">
	            <div class="bar-button red"></div>
	            <div class="bar-button yellow" title="Minimize" role="button"></div>
	            <div class="bar-button green" title="Expand" role="button"></div>
	        </div>
    	    <div class="website">
    	        <?php the_post_thumbnail( 'full' ); ?>
    	    </div>
	    </div>
	    <div class="project-content">
	        <h2 class="entry-title"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h2>
	        <p class="project-type"><?php the_field('project_type'); ?></p>
    	    <div class="project-text"><?php echo the_excerpt(); ?></div>
    	    <a href="<?php echo the_permalink(); ?>">View Project</a>
    	</div>
	</section>
	<?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</article>

<?php get_footer(); ?>