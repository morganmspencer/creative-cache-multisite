<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php if ( has_post_thumbnail() ) : ?>
<div class="featured-image">
	<?php the_post_thumbnail(); ?>
</div>
<?php endif; ?>

<article id="main-content" class="container">
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
		<p class="entry-meta">by <?php the_author();?></p>
		<div class="entry-content">
			<?php
				echo the_content();
			?>
		</div><!-- .entry-content -->
	</div><!-- #post-## -->
</article>
<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>