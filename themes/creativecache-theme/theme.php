<?php
/*
Template Name: Single Theme
*/
?>
<?php get_header(); ?>

<article id="main-content" class="container">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h1 class="entry-title"><?php echo the_title(); ?></h1>
	<section class="website-window">
	    <div class="web-window">
	        <div class="top-bar">
	            <div class="bar-button red"></div>
	            <div class="bar-button yellow"></div>
	            <div class="bar-button green"></div>
	        </div>
    	    <div class="website">
    	        <?php the_post_thumbnail( 'full' ); ?>
    	    </div>
	    </div>
	</section>
	<section>
	    <div class="project-content">
    	    <?php echo the_content(); ?>
    	</div>
	</section>
	<?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</article>

<?php get_footer(); ?>