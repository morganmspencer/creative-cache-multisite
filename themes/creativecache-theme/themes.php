<?php
/*
Template Name: All Themes
*/
?>
<?php get_header(); ?>

<article id="main-content" class="container">
    <h1 class="entry-title"><?php echo the_title(); ?><a href="" class="expand-all" title="Toggle Fullscreen" role="button"><i class="fas fa-expand"></i></a></h1>
	<?php 
		$args = array(
		'post_type' => 'page',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => '_wp_page_template',
				'value' => 'theme.php'
			)
		)
		);
	$the_pages = new WP_Query( $args );

	if( $the_pages->have_posts() ): while( $the_pages->have_posts() ): $the_pages->the_post();
	?>
	<section class="website-window">
	    <div class="web-window minimized">
	        <div class="top-bar">
	            <div class="bar-button red"></div>
	            <div class="bar-button yellow" title="Minimize" role="button"></div>
	            <div class="bar-button green" title="Expand" role="button"></div>
	        </div>
    	    <div class="website">
    	        <?php the_post_thumbnail( 'full' ); ?>
    	    </div>
	    </div>
	    <div class="project-content">
	        <h2 class="entry-title"><?php echo the_title(); ?></h2>
    	    <div class="project-text"><?php echo the_excerpt(); ?></div>
    	    <a href="<?php echo the_permalink(); ?>">View Theme</a>
    	</div>
	</section>
	<?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</article>

<?php get_footer(); ?>