<?php get_header(); ?>

	<main role="main">
		
		<section class="container clear">

			<article id="post-404">

				<h1>Yikes, looks like this page is gone!</h1>
				<p>Sorry about that! But don't worry, I'll keep an eye out for the missing page. In the meantime, search the website or check out one of my recent posts below.</p>
				
				<?php //get_template_part('searchform'); ?>
				
				<h2>Recent Posts</h2>
	
				<?php
					$args = array(
						'numberposts' => 10,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_type' => 'post',
						'post_status' => 'publish'
					);

					$recent_posts = wp_get_recent_posts( $args );
				
					if (count($recent_posts) > 0) :

						foreach( $recent_posts as $recent ){ ?>
					
						<h3 class="post-title"><a href="<?php echo get_permalink($recent["ID"]) ?>"><?php echo $recent["post_title"] ?></a></h3>
					
					<?php 
						} 
						wp_reset_query();
						endif;
					?>

			</article>

		</section>
		
	</main>

<?php get_footer(); ?>
