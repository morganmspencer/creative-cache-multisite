<?php

function ms_adding_scripts() {
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'ms_adding_scripts' );

add_theme_support( 'post-thumbnails' );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
  register_nav_menu('legal-menu',__( 'Legal Menu' ));
}
add_action( 'init', 'register_my_menu' );

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}

// Add Affiliate post type
function create_post_type() {
  register_post_type( 'business_affiliate',
    array(
		'labels' => array(
			'name' => __( 'Affiliates' ),
			'singular_name' => __( 'Affiliate' )
		),
		'public' => true,
        'publicly_queryable' => true,
		'has_archive' => true,
		'hierarchal' => false,
		'rewrite' => array('slug' => 'affiliates'),
        'menu_icon' => 'dashicons-groups',
        'show_in_rest' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    )
  );
}
add_action( 'init', 'create_post_type' );

function create_affiliate_taxonomies() {
	register_taxonomy(
		'affiliate_categories',
		'business_affiliate',
		array(
			'label' => __( 'Categories' ),
			'rewrite' => array( 'slug' => 'affiliates/categories' ),
			'hierarchical' => true,
			'show_in_rest' => true,
		)
	);
}
add_action( 'init', 'create_affiliate_taxonomies' );

//Exclude pages from WordPress Search
if (!is_admin()) {
	function wpb_search_filter($query) {
		if ($query->is_search) {
			$query->set('post_type', 'post');
		}
		return $query;
	}
	add_filter('pre_get_posts','wpb_search_filter');
}

// Threaded comments
function enable_threaded_comments(){
if (!is_admin()) {
     if (is_singular() && comments_open() && (get_option('thread_comments') == 1))
          wp_enqueue_script('comment-reply');
     }
}

add_action('get_header', 'enable_threaded_comments');

function format_comment($comment, $args, $depth) {

	$GLOBALS['comment'] = $comment; ?>

<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

	<div class="comment-intro">
		<em>commented on</em> 
		<a class="comment-permalink" href="<?php echo htmlspecialchars ( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s'), get_comment_date(), get_comment_time()) ?></a>
		<em>by</em> 
		<?php printf(__('%s'), get_comment_author_link()) ?>
	</div>

	<?php if ($comment->comment_approved == '0') : ?>
	<em><php _e('Your comment is awaiting moderation.') ?></em><br />
		<?php endif; ?>

		<?php comment_text(); ?>

		<div class="reply">
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
        
<?php }