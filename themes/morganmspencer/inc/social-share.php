<div id="mms-social" class="container">
	<?php $upload_dir = wp_upload_dir(); ?>
	<ul>
		<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink() ?>" target="_blank" title="Facebook Share" class="facebook"><i class="fab fa-facebook-f fa-fw"></i></a></li>
		<li><a href="http://twitter.com/share?text=<?php echo the_title(); ?>&url=<?php echo the_permalink() ?>" target="_blank" title="Twitter Share" class="twitter"><i class="fab fa-twitter fa-fw"></i></a></li>
		<li><a href="https://pinterest.com/pin/create/button/?url=<?php echo the_permalink() ?>&media=<?php echo $upload_dir['baseurl']; ?>/pinterest/<?php echo $post->post_name; ?>.png&description=<?php echo the_title(); ?>" target="_blank" title="Pinterest Share" class="pinterest"><i class="fab fa-pinterest-p fa-fw"></i></a></li>
		<li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo the_permalink() ?>&title=<?php echo the_title(); ?>&summary=&source=" target="_blank" title="LinkedIn Share" class="linkedin"><i class="fab fa-linkedin-in fa-fw"></i></a></li>
		<li><a href="http://reddit.com/submit?url=<?php echo the_permalink() ?>&title=<?php echo the_title(); ?>" target="_blank" title="Reddit Share" class="reddit"><i class="fab fa-reddit-alien fa-fw"></i></a></li>
		<li><a href="http://www.tumblr.com/share/link?url=<?php echo the_permalink() ?>" target="_blank" title="Tumblr Share" class="tumblr"><i class="fab fa-tumblr fa-fw"></i></a></li>
		<li><a href="https://bufferapp.com/add?text=<?php echo the_title(); ?>&url=<?php echo the_permalink() ?>" target="_blank" title="Buffer Share" class="buffer"><i class="fas fa-layer-group fa-fw"></i></a></li>
		<li><a href="https://web.skype.com/share?url=<?php echo the_permalink() ?>&lang=en-US=" target="_blank" title="Skype Share" class="skype"><i class="fab fa-skype fa-fw"></i></a></li>
		<li><a href="https://getpocket.com/save?url=<?php echo the_permalink() ?>&title=<?php echo the_title(); ?>" target="_blank" title="Pocket Share" class="pocket"><i class="fab fa-get-pocket fa-fw"></i></a></li>
		<li><a href="https://api.whatsapp.com/send?text=<?php echo the_title(); ?>%20<?php echo the_permalink() ?>" target="_blank" title="WhatsApp Share" class="whatsapp"><i class="fab fa-whatsapp fa-fw"></i></a></li>
		<li><a href="https://telegram.me/share/url?url=<?php echo the_permalink() ?>&text=<?php echo the_title(); ?>" target="_blank" title="Telegram Share" class="telegram"><i class="fab fa-telegram-plane fa-fw"></i></a></li>
		<li><a href="javascript:window.print()" title="Print Page" class="print"><i class="fas fa-print fa-fw"></i></a></li>
		<li><a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo the_permalink() ?>" title="Email Share"><i class="far fa-envelope fa-fw"></i></a></li>
		<?php /* <li><a href="#" title="More Share Options" class="more-sharing"><i class="fas fa-plus fa-fw"></i></a></li>
		<li class="extra-sharing"><a href="https://messenger.com/share?link=<?php echo the_permalink() ?>" target="_blank" title="Facebook Messenger Share" class="fb-messenger"><i class="fab fa-facebook-messenger fa-fw"></i></a></li> */ ?>
		<div class="clear"></div>
	</ul>
</div>