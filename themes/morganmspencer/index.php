<?php get_header(); ?>

	<main role="main"<?php if(is_home()): ?> id="homepage"<?php endif; ?>>
		
		<section id="featured-post" class="container clear">

			<?php $the_query = new WP_Query( 'posts_per_page=1' );
			
				while ($the_query -> have_posts()) : $the_query -> the_post(); 
				$category = get_the_category();
			?>
			
			<a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a>
			
			<div class="featured-content">
				
				<span class="date"><?php the_time('F j, Y'); ?></span><a class="category-title" href="<?php echo get_category_link($category[0]->cat_ID) ?>" title="View all posts in <?php echo $category[0]->cat_name ?>"><?php echo $category[0]->cat_name ?></a>
				
				<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				
				<?php if ( has_excerpt()) : ?>
					<?php the_excerpt(); ?>
				<?php endif; ?>
				
			</div>
			
			
			<?php 
			endwhile;
			wp_reset_postdata();
			?>

		</section>
		
		<section id="categories" class="container clear">

			<?php
			
			$recent_posts = wp_get_recent_posts( array( 'numberposts' => '1', 'post_status' => 'publish', ) );
			$recent_post_ID = $recent_posts[0]['ID'];
				
			$cat_args = array(
					'post_status'	=> 'publish',
					'orderby'	 	=> 'date',
					'order' 	 	=> 'ASC',
					);

			$categories = get_categories($cat_args); 

			foreach($categories as $category) {
				
				$cat_count = $category->count;
				
				$post_args = array(
						'posts_per_page' 	=> 1,
						'cat' 				=> $category->cat_ID,
						);

				$posts = query_posts($post_args);

				while(have_posts()) : the_post();
				
				$id = get_the_ID();
				
				if (($cat_count == 1 && $recent_post_ID == $id) || !in_category($category) || ($cat_count > 1 && $recent_post_ID == $id)) {
					// Don't show if only post in category is most recent or if post has same category as most recent
				} else { ?>
					
				<div class="home-category">
					<a class="category-title" href="<?php echo get_category_link( $category->term_id ); ?>" title="View all posts in <?php echo $category->name; ?>"><?php echo $category->name; ?></a>
					<a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail()) : ?><?php the_post_thumbnail(); ?><?php endif; ?></a>
					<span class="date"><?php the_time('F j, Y'); ?></span>
					<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<?php if ( has_excerpt()) : ?>
						<div class="home-excerpt">
							<?php //the_excerpt(); ?>
						</div>
					<?php endif; ?>
				</div>
				<?php }
				endwhile;
				wp_reset_postdata();
			}
?>

		</section>
		
		<?php
		
		$categories = get_categories(); 
		
		$post_count = 0;
 
		foreach ( $categories as $category ) {
			if ($category->count > 1) {
				$post_count = $post_count + 1;
			}
		}
		
		if ($post_count > 0) :
		?>
		
		<section id="other-posts" class="container clear">
			<h2>Recent Posts</h2>
			<div>
			
			<?php
				// Get post ids of first post in each category
				// Get all posts except first posts
			?>
			
			<?php
				
				$first_posts = array();
				
				foreach ( $categories as $category ) {
					
					if ($category->cat_ID == $most_recent_cat) {
						$post_args = array(
							'posts_per_page' => 2,
							'cat' => $category->cat_ID,
							);

						$posts = query_posts($post_args);

						while(have_posts()) : the_post();

						$id = get_the_ID();
						$first_posts[] = $id;

						endwhile;
						wp_reset_postdata();
					} else {
						$post_args = array(
							'posts_per_page' => 1,
							'cat' => $category->cat_ID,
							);

						$posts = query_posts($post_args);

						while(have_posts()) : the_post();

						$id = get_the_ID();
						$first_posts[] = $id;

						endwhile;
						wp_reset_postdata();
					}
				}
				
				$post_args = array(
					'posts_per_page' => 12,
					'post__not_in'	 => $first_posts,
				);

				$posts = query_posts($post_args);

				while(have_posts()) : the_post();
				
				$category = get_the_category();
			?>
			
			<div class="home-category">
				<a class="category-title" href="<?php echo get_category_link($category[0]->cat_ID) ?>" title="View all posts in <?php echo $category[0]->cat_name ?>" ><?php echo $category[0]->cat_name ?></a>
				<a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail()) : ?><?php the_post_thumbnail(); ?><?php endif; ?></a>
				<span class="date"><?php the_time('F j, Y'); ?></span>
				<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php if ( has_excerpt()) : ?>
				<div class="home-excerpt">
					<?php //the_excerpt(); ?>
				</div>
				<?php endif; ?>
			</div>
			
			<?php endwhile; wp_reset_postdata(); ?>
				
			</div>
			
		</section>
		
		<?php endif; ?>

	</main>

<?php get_footer(); ?>
