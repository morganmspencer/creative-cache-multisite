<?php get_header(); ?>

	<main role="main">
		
		<section class="container clear">

			<h1>Results for "<?php echo get_search_query(); ?>"</h1>

			<div class="archive-content">
				<?php get_template_part('loop'); ?>
				<div class="nav-previous alignleft"><?php previous_posts_link( 'Older posts' ); ?></div>
<div class="nav-next alignright"><?php next_posts_link( 'Newer posts' ); ?></div>
			</div>
			
			<?php get_sidebar(); ?>
			<div class="clear"></div>

		</section>
		
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
