<?php get_header(); ?>

<main role="main">
	
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<div class="container">
				
				<span class="date"><?php the_time('F j, Y'); ?></span>
				<span class="category"><?php the_category(', '); ?></span>
				<span class="sep">|</span>
				<span class="comments"><a href="#respond"><?php echo comments_number('No Comments', '1 Comment', '% Comments'); ?></a></span>
				
			</div>
			
			<h1 class="container"><?php the_title(); ?></h1>
			
			<?php get_template_part( 'inc/social', 'share' ); ?>
			
			<?php if ( has_post_thumbnail()) : ?>
				<div class="post-banner container">
				<?php the_post_thumbnail(); ?>
				</div>
			<?php endif; ?>
			
			<section class="container">
				
				<div class="entry-content">
					
					<?php if ( function_exists( 'sharing_display' ) ) {
						sharing_display( '', true );
					}

					if ( class_exists( 'Jetpack_Likes' ) ) {
							$custom_likes = new Jetpack_Likes;
							echo $custom_likes->post_likes( '' );
					} ?>
					
					<?php the_content(); ?>
					
					<?php get_template_part( 'inc/social', 'share' ); ?>
					
					<?php if (has_tag()): ?>
					<div class="post-meta">
						<span class="tags"><?php the_tags(); ?></span>
					</div>
					<?php endif; ?>
					
					<?php

						$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 2, 'post__not_in' => array($post->ID) ) );
						if( $related ) {
						
					?>
					<section id="related-posts">
					<h3>Related Post<?php if (count($related) > 1): ?>s<?php endif; ?></h3>
					<div>
					<?php
						foreach( $related as $post ) {
						setup_postdata($post);
					?>
						<div class="home-category related-post">
							<a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail()) : ?><?php the_post_thumbnail(); ?><?php endif; ?></a>
							<span class="date"><?php the_time('F j, Y'); ?></span>
							<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</div>
					
					<?php
							}
					?>
						</div>
					</section>
					<?php
						}
						wp_reset_postdata();
					?>
					
					<div class="entry-comments">
						<?php comments_template(); ?>
					</div>
					
				</div>
				
				<?php get_sidebar(); ?>
				
			</section>
				

		</article>

	<?php endwhile; ?>

	<?php else: ?>

		<article>

			<h1>Sorry, nothing to display. Content coming soon!</h1>

		</article>

	<?php endif; ?>
		
	<div class="clear"></div>

	</section>
	
</main>

<?php get_footer(); ?>
