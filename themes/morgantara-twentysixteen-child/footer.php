		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">

			<div class="site-info">
				&copy; <?php echo date("Y"); ?> <?php if (new DateTime() < new DateTime('November 04 2018 17:30:00 UTC-0500')) { ?>Morgan Spencer &amp; Tara Hennig<?php } else { ?> Morgan &amp; Tara Spencer<?php } ?>, All Rights Reserved | Designed, developed and hosted by <a href="https://creativecache.us/" alt="Creative Cache" target="_blank">Creative Cache</a>
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<script type="text/javascript">
	function getTimeRemaining(endtime) {
		var t = Date.parse(endtime) - Date.parse(new Date());
		var seconds = Math.floor((t/1000) % 60);
		var minutes = Math.floor((t/1000/60) % 60);
		var hours = Math.floor((t/(1000*60*60)) % 24);
		var days = Math.floor(t/(1000*60*60*24));
		return {
			'total': t,
			'days': days,
			'hours': hours,
			'minutes': minutes,
			'seconds': seconds
		};
	};

	function initializeClock(id, endtime) {
		var clock = document.getElementById(id);
		var daysSpan = clock.querySelector('.days');
		var hoursSpan = clock.querySelector('.hours');
		var minutesSpan = clock.querySelector('.minutes');
		var secondsSpan = clock.querySelector('.seconds');
		function updateClock() {
			var t = getTimeRemaining(endtime);
			daysSpan.innerHTML = t.days;
			hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
			minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
			secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
			if(t.total <= 0) {
				clearInterval(timeinterval);
				
				var timeToHide = document.getElementsByClassName('time-segment');

				if (t.total >= -18000000) {
					for(var i = 0; i < timeToHide.length; i++){
						timeToHide[i].style.display = "none";
					}
					document.getElementById('cc-countdown').innerHTML = "We're getting married now!";
				} else {
					for(var i = 0; i < timeToHide.length; i++){
						timeToHide[i].style.display = "none";
					}
					document.getElementById('cc-countdown').innerHTML = "We're married!";
				};
			};
		};

		updateClock();
		var timeinterval = setInterval(updateClock,1000);
	};

	var deadline = 'November 04 2018 17:00:00 UTC-0500'
	initializeClock('cc-countdown', deadline);



	// var menuItem = document.getElementsByClassName('menu-item');
	// console.log(menuItem);

	// for(var m = 0; m < menuItem.length; m++) {
	// 	if (menuItem[m].className != 'menu-item-home') {
	// 		menuItem[m].onclick = function() {
	// 			var element = document.getElementById('large-header');
	// 			element.classList.add('small-header');
	// 		};
	// 	}
	// }

</script>

<?php wp_footer(); ?>
</body>
</html>