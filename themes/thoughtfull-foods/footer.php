<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Thoughtfull_Foods
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			&copy; <a href="<?php echo site_url(); ?>"><?php echo bloginfo('name'); ?></a>, All Rights Reserved
			<span class="sep"> | </span>
			designed and developed by <a href="https://creativecache.co/">Creative Cache</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
